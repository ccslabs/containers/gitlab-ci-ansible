FROM alpine:edge
LABEL MAINTAINER=jpward1981@gmail.com

RUN apk update --no-cache && apk upgrade --no-cache
RUN apk add --no-cache ansible ansible-lint py3-pip py3-xmltodict
RUN apk add py3-ncclient --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/


RUN addgroup -g 2001 ansible
RUN adduser -G ansible -D -h /home/ansible -s /bin/ash -u 2001 ansible
RUN chown ansible:ansible -R /home/ansible

USER ansible
CMD ["ansible" "--version"]
